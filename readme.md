# Chrome Extension to copy OAuth-Tokens from SugarCRM
Sick and tired of searching for and copying your OAuth-Token from a SugarCRM environment to use in a RESTfull client? Then this extension is for you! Okay, now on a more serious note.

When you have more then one SugarCRM environment you are working on and you want to test some endpoints using your REST-client, you may have noticed that you need an OAuth-Token for that. You would have to retrieve it manually from your LocalStorage in the browser or first do an authentication call or ofcourse make a Pre-Request script that signs you in and retrieves a token for you. But that can be tedious or has to be changed everytime to match the environment you are testing on. When you just want to have it now!

That is why I created this very small and simple Chrome Extension to just copy the OAuth-Token from your current SugarCRM tab straight to the clipboard, ready to be pasted in your REST client.

## How to install?
1. Open Google Chrome and go to chrome://extensions 
2. Make sure `developer mode` is enabled.
3. Drag the entire directory (one-up from this file) into that window
4. Now you have a nice new SugarCRM icon next to your browser URL
5. All set

### Using Firefox
This extension is also compatible with Firefox. To start using this extension, do the following:
1. Open firefox and go to about:debugging
2. On the left choose "This Firefox" (for Firefox Quantum only)
3. Press the button "Load temporary add-on"
4. Browse to the extension directory and click on the manifest.json
5. All set (just ignore the unexpected property warning)

### Updating
You can simply update the extension by pulling in the latest changes (or downloading the latest zip). Go to the same pages as instructed above and press the reload button. Any open SugarCRM pages need to either be reloaded or reopened, depending on your browser of choice.

### Roadmap
Upcoming or nice-to-have features for the future.

- Settings dropdown/page to configure the key in which the OAuth-Token is stored
- Any feedback or requests are welcome, but no guarantees!