/*
MIT License

Copyright (c) 2019 Sven E. Stolk

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
chrome.runtime.onMessage.addListener(
	function (request, sender, sendResponse) {
		if (request.message !== "copy_token") {
			return true;
		}

		let accessToken = window.localStorage.getItem('prod:SugarCRM:AuthAccessToken');
		if (accessToken === null || accessToken === '') {
			sendResponse({error: chrome.i18n.getMessage('errorTokenNotFound')});

			return true;
		}

		accessToken = accessToken.substring(1);
		accessToken = accessToken.substring(0, accessToken.length - 1);

		sendResponse(accessToken);

		return true;
	}
);
