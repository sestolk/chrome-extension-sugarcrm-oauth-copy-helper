document.addEventListener('DOMContentLoaded', function () {
	chrome.tabs.query({active: true}, function (tabs) {
		const resultEle = document.getElementById('result');
		const tokenEle = document.getElementById('token');

		if (!(/SugarCRM/.test(tabs[0].title))) {
			resultEle.className = resultEle.className + ' error';
			resultEle.innerText = chrome.i18n.getMessage('errorInvalidTab');
			return;
		}

		chrome.tabs.sendMessage(
			tabs[0].id,
			{"message": "copy_token"},
			{},
			function (response) {
				resultEle.className = resultEle.className
					.replace('error', '')
					.replace('success', '');
				tokenEle.innerText = '';

				if (typeof response === "undefined") {
					resultEle.className = resultEle.className + ' error';
					resultEle.innerText = 'Unknown error occurred!';
					return;
				}

				if (typeof response.error !== "undefined") {
					resultEle.className = resultEle.className + ' error';
					resultEle.innerText = response.error;
					return;
				}

				tokenEle.innerText = response;
				resultEle.className = resultEle.className + ' success';

				navigator.clipboard.writeText(response).then(function () {
					resultEle.innerText = chrome.i18n.getMessage('successTokenCopied');
				}, function () {
					resultEle.innerText = chrome.i18n.getMessage('errorTokenCopied');
				});
			});
	});
});
